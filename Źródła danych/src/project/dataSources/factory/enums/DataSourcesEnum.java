package project.dataSources.factory.enums;

public enum DataSourcesEnum {
	XML,
    DB,
    WebService,
}
