package project.dataSources.factory.enums;

import java.util.HashMap;
import java.util.Map;

import project.dataSources.factory.Factory;
import project.dataSources.factory.sources.DBFactory;
import project.dataSources.factory.sources.WebServiceFactory;
import project.dataSources.factory.sources.XMLFactory;

public enum DAOFactoryProvider {
	INSTANCE;

    private static Factory factory;

    private Map<DataSourcesEnum, Factory> dataSourcesMap;

    {
    	dataSourcesMap = new HashMap<DataSourcesEnum, Factory>();
    	dataSourcesMap.put(DataSourcesEnum.XML, new XMLFactory());
    	dataSourcesMap.put(DataSourcesEnum.DB, new DBFactory());
    	dataSourcesMap.put(DataSourcesEnum.WebService, new WebServiceFactory());
    }

    public void setSource(DataSourcesEnum factoryEnum) {
        factory = dataSourcesMap.get(factoryEnum);
    }

    public static  Factory getService() {
        return factory;
    }

}
