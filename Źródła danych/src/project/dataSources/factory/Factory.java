package project.dataSources.factory;

public interface Factory {
	
	public int getAge();
	
	public void setAge(int age);
	
	public String getName();
	
	public void setName(String name);
	
	public String getCity();
	
	public void setCity(String city);
	
}
