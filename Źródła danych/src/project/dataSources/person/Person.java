package project.dataSources.person;

import project.dataSources.factory.Factory;

public class Person {
	private String name;
    private int age;
    private String city;
    
    public Person(Factory factory) {
    	this.name = factory.getName();
    	this.age = factory.getAge();
    	this.city = factory.getCity();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age + '\'' +
                ", city=" + city +

                '}';
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
