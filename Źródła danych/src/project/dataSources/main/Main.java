package project.dataSources.main;

import project.dataSources.factory.enums.DAOFactoryProvider;
import project.dataSources.factory.enums.DataSourcesEnum;
import project.dataSources.factory.sources.DBFactory;
import project.dataSources.factory.sources.XMLFactory;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DAOFactoryProvider.INSTANCE.setSource(DataSourcesEnum.XML);
		System.out.println(DAOFactoryProvider.getService() instanceof XMLFactory);
		DAOFactoryProvider.INSTANCE.setSource(DataSourcesEnum.DB);
		System.out.println(DAOFactoryProvider.getService() instanceof XMLFactory);
		System.out.println(DAOFactoryProvider.getService() instanceof DBFactory);


	}

}
