package project.bank.classes;

public class Operation {
	private String accountNumber;
	private int money;
	private String title;
	
	public Operation(String accountNumber, int money, String title) {
		this.accountNumber = accountNumber;
		this.money = money;
		this.title = title;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return accountNumber + "`" + money + "`" + title;
	}
	
	public String operationDescribe() {
		//String moneyString = Integer.toString(money);
		if(money > 0) {
			return "Account number: " + accountNumber + "   Money amount: +" + money + "   Title: " + title;
		}
		else {
			return "Account number: " + accountNumber + "   Money amount: " + money + "   Title: " + title;
		}
	}

}
