package project.bank.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {
	private String name;
	private String surname;
	private String pesel;
	private Date birthdate;
	private List<Account> accounts = new ArrayList<>();
	private List<Operation> personOperations = new ArrayList<>();
	
	public Person(String name, String surname, String pesel, String birthdate) {
		this.name = name;
		this.surname = surname;
		this.pesel = pesel;
		this.birthdate = stringToDate(birthdate);
	}
	
	public Person(String name, String surname, String pesel, Date birthdate) {
		this.name = name;
		this.surname = surname;
		this.pesel = pesel;
		this.birthdate = birthdate;
	}
	
	private Date stringToDate(String dateInString) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = formatter.parse(dateInString);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("You entered wrong date");
		}
		return null;
	}
	
	private String dateToString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.format(date);
	}

	public void addPersonOperation(Operation operation) {
		personOperations.add(operation);
	}
	
	public void personDetails() {
		System.out.println(this.personDescribe());
		System.out.println("\nAccounts list:");
		this.accountDescribe();
		System.out.println("\nOperations history:");
		for(Operation operation : personOperations) {
			System.out.println(operation.operationDescribe());
		}
	}
	
	public String personDescribe() {
		return "Name: " + name + "   Surname: " + surname + "   Pesel: " + pesel + "   Birthdate: " + this.dateToString(birthdate);
	}
	
	public void accountDescribe() {
		for(Account account1 : accounts) {
			System.out.println("Account number: " + account1.getAccountNumber() + "   Balance: " + Integer.toString(account1.getAccountBalance()));
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public void getAccounts() {
		for(Account account1 : accounts) {
			System.out.println(account1.getAccountNumber());
		}
	}
	
	public void addAccount(Account account) {
		accounts.add(account);
	}

	@Override
	public String toString() {
		return name + " " + surname + " " + pesel + " " + this.dateToString(birthdate);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (pesel == null) {
			if (other.pesel != null)
				return false;
		} else if (!pesel.equals(other.pesel))
			return false;
		return true;
	}


	
	
	
	

}
