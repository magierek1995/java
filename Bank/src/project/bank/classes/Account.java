package project.bank.classes;

import java.util.ArrayList;
import java.util.List;

public class Account {
	private String accountNumber;
	private int accountBalance;
	private List<Operation> accountOperations = new ArrayList<>();
	private String personPesel;
	
	
	public Account(String accountNumber, String personPesel) {
		this.accountNumber = accountNumber;
		this.personPesel = personPesel;
		this.accountBalance = 1000;
	}
	
	public Account(String accountNumber, int accountBalance, String personPesel) {
		this.accountNumber = accountNumber;
		this.personPesel = personPesel;
		this.accountBalance = accountBalance;
	}


	public void setAccountBalance(int money) {
		if(this.accountBalance + money < 0)
			throw new IllegalArgumentException();
		this.accountBalance += money;
	}
	
	public void accountDetails() {
		System.out.println(this.accountDescribe());
		System.out.println("\nOperations history:");
		this.operationDescribe();
	}
	
	public String accountDescribe() {
		return "Account number: " + accountNumber + "   Balance: " + Integer.toString(accountBalance) + "   User pesel: " + personPesel;
	}
	
	public void operationDescribe() {
		for(Operation operation1 : accountOperations) {
			if(operation1.getMoney() > 0) {
				System.out.println("Money amount: +" + operation1.getMoney() + "   Title: " + operation1.getTitle());
			}
			else {
				System.out.println("Money amount: " + operation1.getMoney() + "   Title: " + operation1.getTitle());
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		return true;
	}

	public String getAccountNumber() {
		return accountNumber;
	}


	@Override
	public String toString() {
		return accountNumber + " " + accountBalance + " " + personPesel;
	}


	public int getAccountBalance() {
		return accountBalance;
	}


	public List<Operation> getAccountOperations() {
		return accountOperations;
	}
	
	public void addAccountOperation(Operation operation) {
		accountOperations.add(operation);
	}


	public String getPersonPesel() {
		return personPesel;
	}
	
	

}
