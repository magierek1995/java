package project.bank.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import project.bank.classes.Account;
import project.bank.classes.Operation;
import project.bank.classes.Person;


public class BankControl {
    private Scanner scanner = new Scanner(System.in);
    private List<Account> accounts = new ArrayList<Account>();
    private List<Person> persons = new ArrayList<Person>();
    private List<Operation> operations = new ArrayList<Operation>();

	public BankControl() {		
	}
	
	public boolean bankMenu() throws IOException, RuntimeException {
		System.out.println("\n************************");
        System.out.println("1. Create person");
        System.out.println("2. Create account");
        System.out.println("3. Deposit cash");
        System.out.println("4. Withdraw cash from account");
        System.out.println("5. Withdraw cash to other account");
        System.out.println("6. International transaction");
        System.out.println("7. Show bank operations history");
        System.out.println("8. Show user details");
        System.out.println("9. Show account details");
        System.out.println("10. Edit person");
        System.out.println("11. View persons");
        System.out.println("12. View accounts");
        System.out.println("13. Exit\n");
        System.out.print("Enter your choice: ");
        
        int selectedNumber = scanner.nextInt();
        if(selectedNumber == 13) {
        	saveData();
        	return false;
        }
        action(selectedNumber);
        return true;
    }
	
	private void action(int selectedNumber) throws IOException, RuntimeException {
        switch (selectedNumber) {
            case 1:
                createUser();
                break;
            case 2:
                createAccount();
                break;
            case 3:
                oneAccountOperation(true);
                break;
            case 4:
            	oneAccountOperation(false);
                break;
            case 5:
            	twoAccountsOperation();
                break;
            case 6:
                //internationalTransfer();
                break;
            case 7:
                bankOperationsHistory();
                break;
            case 8:
                userDetails();
                break;
            case 9:
                accountDetails();
                break;
            case 10:
                editUser();
                break;
            case 11:
                viewPersons();
                break;
            case 12:
                viewAccounts();
                break;
            default:
                break;
        }
    }
	
	private void bankOperationsHistory() {
		for(Operation operation1 : operations) {
			System.out.println(operation1.operationDescribe());
		}
	}
	
	private void userDetails() {
		System.out.println("Enter user id: ");
		viewPersons();
		int id = scanner.nextInt();
		persons.get(id).personDetails();		
	}
	
	private void accountDetails() {
		System.out.println("Enter account id");
		viewAccounts();
		int id = scanner.nextInt();
		accounts.get(id).accountDetails();
	}
	
	private void twoAccountsOperation() {
		try {
		System.out.println("Whereof account do you want to transfer? ");
		viewAccounts();
		int firstAccount = scanner.nextInt();
		System.out.println("To which account do you want transfer to ?");
		viewAccounts();
		int secondAccount = scanner.nextInt();
		System.out.println("How much money do you want transfer to? ");
		int money = scanner.nextInt();
		System.out.println("Enter deposit title: ");
		String title = scanner.next();
		addTwoAccountOperation(firstAccount, secondAccount, money, title);
		}
		catch(IllegalArgumentException e) {
			System.out.println("You entered wrong data");
		}
		
	}
	
	private void oneAccountOperation(boolean deposit) {
		try {
			if(deposit) {
				System.out.println("To which account do you want to deposit? ");
				viewAccounts();
				int account = scanner.nextInt();
				System.out.println("How much money do you want to deposit? ");
				int money = scanner.nextInt();
				if (money < 0)
		            throw new IllegalArgumentException();
				System.out.println("Enter deposit title: ");
				String title = scanner.next();
				addOneAccountOperation(account, money, title);
			}
			else {
				System.out.println("Whereof account do you want to withdraw?");
				viewAccounts();
				int account = scanner.nextInt();
				System.out.println("How much money do you want to withdraw? ");
				int money = scanner.nextInt();
				if (money < 0)
		            throw new IllegalArgumentException();
				System.out.println("Enter withdraw title: ");
				String title = scanner.next();
				addOneAccountOperation(account, -money, title);
			}
		}
		catch(IllegalArgumentException e) {
			System.out.println("You entered wrong data");
		}
	}
	
	private void addTwoAccountOperation(int firstAccount, int secondAccount, int money, String title) {
		Account first = null, second = null;
		first = accounts.get(firstAccount);
		second = accounts.get(secondAccount);
		first.setAccountBalance(-money);
		second.setAccountBalance(money);
		Operation operation1 = new Operation(first.getAccountNumber(), -money, title);
		Operation operation2 = new Operation(second.getAccountNumber(), money, title);
		operations.add(operation1);
		operations.add(operation2);
		first.addAccountOperation(operation1);
		second.addAccountOperation(operation2);
		for(Person person1 : persons) {
			if (person1.getPesel().equals(first.getPersonPesel())){
				person1.addPersonOperation(operation1);
			}
			if (person1.getPesel().equals(second.getPersonPesel())){
				person1.addPersonOperation(operation2);
			}
		}
	}
	
	private void addOneAccountOperation(int account, int money, String title) {
		Account account1 = accounts.get(account);
		account1.setAccountBalance(money);
		Operation operation = new Operation(account1.getAccountNumber(), money, title);
		operations.add(operation);
		account1.addAccountOperation(operation);
		for(Person person1 : persons) {
			if (person1.getPesel().equals(account1.getPersonPesel())){
				person1.addPersonOperation(operation);
			}
		}
	}
	
    private void createUser() {
    	try {
	    	System.out.println("Enter your name: ");
	        String name = scanner.next();
	        System.out.println("Enter your surname: ");
	        String surname = scanner.next();
	        System.out.println("Enter your PESEL");
	        String pesel = scanner.next();
	        System.out.println("Enter your birthdate (dd/mm/yyyy)");
	        String birthdate = scanner.next();
	        if(pesel.length() != 11)
	        	throw new IllegalArgumentException();
	        peselCheck(pesel);
	        Date birth = stringToDate(birthdate);
	        if(birth != null) {
		        Person person = new Person(name, surname, pesel, birth);
		        persons.add(person);
	        }
    	}
    	catch(IllegalArgumentException e){
    		System.out.println("You entered wrong data");
    	}
    }
    
    private Date stringToDate(String dateInString) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = formatter.parse(dateInString);
			return date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("You entered wrong date");
		}
		return null;
	}
    
    private void createAccount() {
    	try {
	    	System.out.println("Which person do you want to create an account? ");
	    	viewPersons();
	    	int id = scanner.nextInt();
	    	System.out.println("Account number (18 digits): ");
	    	String number = scanner.next();
	    	if(number.length() != 18)
	    		throw new IllegalArgumentException();
	    	numberCheck(number);
	    	Account account = new Account(number, persons.get(id).getPesel());
	    	accounts.add(account);
	    	persons.get(id).addAccount(account);
    	}
    	catch(IllegalArgumentException e) {
    		System.out.println("You entered wrong data");
    	}
    }
    
    private void numberCheck(String number) {
    	for(Account account1 : accounts) {
    		if(number.equals(account1.getAccountNumber()))
    			throw new IllegalArgumentException();
    	}
    }
    
    private void peselCheck(String pesel) {
    	for(Person person1 : persons) {
    		if(pesel.equals(person1.getPesel()))
    			throw new IllegalArgumentException();
    	}
    }
    
    private void editUser() {
    	System.out.println("Which person do you want to edit? ");
    	viewPersons();
    	int id = scanner.nextInt();
    	System.out.println("Enter new name: ");
    	String name = scanner.next();
    	System.out.println("Enter new surname: ");
    	String surname = scanner.next();
    	System.out.println("Enter new birthdate (dd/mm/yyyy): ");
    	String birthdate = scanner.next();
    	Date birth = stringToDate(birthdate);
        if(birth != null) {
	    	persons.get(id).setName(name);
	    	persons.get(id).setSurname(surname);
	    	persons.get(id).setBirthdate(birth);
        }
    }
    
    
    private void saveOperations() throws IOException {
    	try {
            FileWriter writer = new FileWriter("operations.txt", false);
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.write("");
            printWriter.close();
        } catch (IOException ex) {
            throw ex;
        }   	
        try {
            FileWriter writer = new FileWriter("operations.txt", true);
            PrintWriter printWriter = new PrintWriter(writer);
            for(Operation operation1 : operations) {
            	printWriter.write(operation1.toString() + "\r\n");
            }
            printWriter.close();
        } catch (IOException ex) {
            throw ex;
        }
    }
    
    private void saveAccounts() throws IOException, RuntimeException {
    	try {
            FileWriter writer = new FileWriter("accounts.txt", false);
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.write("");
            printWriter.close();
        } catch (IOException ex) {
            throw ex;
        }   	
        try {
            FileWriter writer = new FileWriter("accounts.txt", true);
            PrintWriter printWriter = new PrintWriter(writer);
            for(Account account1 : accounts) {
            	printWriter.write(account1.toString() + "\r\n");
            }
            printWriter.close();
        } catch (IOException ex) {
            throw ex;
        }
    }
    
    private void savePersons() throws IOException, RuntimeException {
    	try {
            FileWriter writer = new FileWriter("persons.txt", false);
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.write("");
            printWriter.close();
        } catch (IOException ex) {
            throw ex;
        }	
        try {
            FileWriter writer = new FileWriter("persons.txt", true);
            PrintWriter printWriter = new PrintWriter(writer);
            for(Person person1 : persons) {
            	printWriter.write(person1.toString() + "\r\n");
            }
            printWriter.close();
        } catch (IOException ex) {
            throw ex;
        }
    }
    
    private void loadPersons() {
    	try {
            File file = new File("persons.txt");
            Scanner input = new Scanner(file);
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String[] tab = null;
                tab = line.split(" ");
                Person person = new Person(tab[0], tab[1], tab[2], tab[3]);
                persons.add(person);
            }
            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }    
    
    private void loadOperations() {
    	try {
            File file = new File("operations.txt");
            Scanner input = new Scanner(file);
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String[] tab = null;
                tab = line.split("`");
                int money = Integer.parseInt(tab[1]);
                Operation operation = new Operation(tab[0], money, tab[2]);
                operations.add(operation);
                for(Account account1 : accounts) {
                	if(account1.getAccountNumber().equals(operation.getAccountNumber())) {
                		account1.addAccountOperation(operation);
                		for(Person person1 : persons) {
                			if(person1.getPesel().equals(account1.getPersonPesel())) {
                				person1.addPersonOperation(operation);
                			}
                		}               	
                	}
                }
            }
            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void loadAccounts() {
    	try {
            File file = new File("accounts.txt");
            Scanner input = new Scanner(file);
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String[] tab = null;
                tab = line.split(" ");
                int balance = Integer.parseInt(tab[1]);
                Account account = new Account(tab[0], balance, tab[2]);
                accounts.add(account);
            }
                for(Account account1 : accounts) {
                	for(Person person1 : persons) {
                		if(person1.getPesel().equals(account1.getPersonPesel())) {
                			person1.addAccount(account1);                	
                		}
                	}
                }
            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void viewPersons() {
    	int iterator = 0;
    	for(Person person1 : persons) {
    		System.out.println(iterator++ + ". " + person1.toString());
    	}
    }
    
    private void viewAccounts() {
    	int iterator = 0;
    	for(Account account1 : accounts) {
    		System.out.println(iterator++ + ". " + account1.accountDescribe());
    	}
    }
    
    public void saveData() throws IOException, RuntimeException {
    	saveOperations();
    	saveAccounts();
    	savePersons();
    }
    
    public void loadData() {
    	loadPersons();
    	loadAccounts();
    	loadOperations();
    }
}
