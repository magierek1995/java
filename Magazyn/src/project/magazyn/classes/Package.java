package project.magazyn.classes;

import java.text.SimpleDateFormat;
import java.util.Date;

import project.magazyn.enums.TypeOfPackage;

public class Package {
	private String describe = "qwerty";
	private int packageNumber;
	private int priority;
	private int moves;
	private Date date = new Date();
	private int x;
	private int y;
	private int z;
	private TypeOfPackage type = TypeOfPackage.valueOf("Toys");
	
	
	public String coordinatesToString() {
		return x + " " + y + " " + z;
	}
	
	public TypeOfPackage getTyp() {
		return type;
	}

	public void setTyp(TypeOfPackage type) {
		this.type = type;
	}	

	public Package(int priority, int number, int x, int y, int z) {
		this.priority = priority;
		this.packageNumber = number;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	private String dateToString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.format(date);
	}

	@Override
	public String toString() {
		return "Package [describe=" + describe + ", packageNumber=" + packageNumber + ", priority=" + priority
				+ ", moves=" + moves + ", date=" + this.dateToString(date) + ", x=" + x + ", y=" + y + ", z=" + z + ", typ=" + type + "]";
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getNumber() {
		return packageNumber;
	}

	public void setNumber(int number) {
		this.packageNumber = number;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public int getPackageNumber() {
		return packageNumber;
	}

	public void setPackageNumber(int packageNumber) {
		this.packageNumber = packageNumber;
	}

	public int getMoves() {
		return moves;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TypeOfPackage getType() {
		return type;
	}

	public void setType(TypeOfPackage type) {
		this.type = type;
	}
	
	public void addMove() {
		this.moves++;
	}

}
