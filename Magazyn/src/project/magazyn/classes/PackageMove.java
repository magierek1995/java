package project.magazyn.classes;

public class PackageMove {
	int number;
	String oldCoordinates;
	String newCoordinates;
	
	@Override
	public String toString() {
		return "PackageMove [number=" + number + ", oldCoordinates=" + oldCoordinates + ", newCoordinates="
				+ newCoordinates + "]";
	}
	public PackageMove(int number, String oldCoordinates, String newCoordinates) {
		this.number = number;
		this.oldCoordinates = oldCoordinates;
		this.newCoordinates = newCoordinates;
	}

	
	
}
