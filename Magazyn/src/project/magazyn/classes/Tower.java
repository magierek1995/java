package project.magazyn.classes;

import java.util.ArrayList;
import java.util.List;

public class Tower {
	private List<Package> packages = new ArrayList<>();
	private int a = 5;
	
	public List<Package> getPackages() {
		return packages;
	}

	public void setAaa(int a, int b) {
		//aaa.set(a, b);
	}
	
	public int getA() {
		return a;
	}
	
	public void addPackage(Package package1) {
		packages.add(package1);
	}
	
	public int getSize() {
		return packages.size();
	}
	
	public void deletePackage() {
		packages.remove(packages.size() - 1);
	}
	
	public Package lastItem() {
		return packages.get(packages.size() - 1);
	}
	
	public String getPriority() {
		String get = "[";
		for(Package paczka : packages) {
			get += paczka.getPriority() + ", ";
		}
		get += "]";
		return get;
	}
	
	public String getNumber() {
		String get = "[";
		for(Package paczka : packages) {
			get += paczka.getNumber() + ", ";
		}
		get += "]";
		return get;
	}
	
}
