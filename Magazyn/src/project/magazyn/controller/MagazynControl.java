package project.magazyn.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import project.magazyn.classes.Package;
import project.magazyn.classes.PackageMove;
import project.magazyn.classes.Tower;
import project.magazyn.enums.TypeOfPackage;

public class MagazynControl {
	private Scanner scanner = new Scanner(System.in);
	private static List<Package> packages = new ArrayList<>();
	private List<PackageMove> movesHistory = new ArrayList<>();
	private static Tower[][] tower = new Tower[4][3];
	
	public MagazynControl() {
		for(int i = 0; i < tower.length; i++) {
			for(int j = 0; j < tower[i].length; j++) {
				tower[i][j] = new Tower();
			}
		}
	}
	
	
	//nie zdążyłem zaimplementować metod wypełniających magazyn, dlatego wypełniłem go ręcznie
	public void loadDataDoZrobienia() {
		int iterator = 1;
		tower[0][0].addPackage(new Package(1, iterator++, 0, 0, tower[0][0].getSize()));
		tower[0][0].addPackage(new Package(3, iterator++, 0, 0, tower[0][0].getSize()));
		tower[0][1].addPackage(new Package(1, iterator++, 0, 1, tower[0][1].getSize()));
		tower[0][1].addPackage(new Package(1, iterator++, 0, 1, tower[0][1].getSize()));
		tower[0][1].addPackage(new Package(2, iterator++, 0, 1, tower[0][1].getSize()));
		tower[1][0].addPackage(new Package(2, iterator++, 1, 0, tower[1][0].getSize()));
		tower[1][0].addPackage(new Package(2, iterator++, 1, 0, tower[1][0].getSize()));
		tower[1][1].addPackage(new Package(1, iterator++, 1, 1, tower[1][1].getSize()));
		tower[1][1].addPackage(new Package(1, iterator++, 1, 1, tower[1][1].getSize()));
		tower[1][1].addPackage(new Package(2, iterator++, 1, 1, tower[1][1].getSize()));
		tower[2][1].addPackage(new Package(1, iterator++, 2, 1, tower[2][1].getSize()));
		tower[2][1].addPackage(new Package(1, iterator++, 2, 1, tower[2][1].getSize()));
		tower[2][1].addPackage(new Package(1, iterator++, 2, 1, tower[2][1].getSize()));
		tower[3][0].addPackage(new Package(1, iterator++, 3, 0, tower[3][0].getSize()));
		tower[3][1].addPackage(new Package(1, iterator++, 3, 1, tower[3][1].getSize()));
		tower[3][1].addPackage(new Package(1, iterator++, 3, 1, tower[3][1].getSize()));
		tower[3][2].addPackage(new Package(1, iterator++, 3, 2, tower[3][2].getSize()));
		tower[3][2].addPackage(new Package(3, iterator++, 3, 2, tower[3][2].getSize()));
		tower[0][1].addPackage(new Package(3, iterator++, 0, 1, tower[0][1].getSize()));

		addPackagesToList();
		
		for(Package package1 : packages) {
			package1.setTyp(TypeOfPackage.chooseEnum(package1.getNumber() % 3 + 1));
		}
	}
	
	public boolean magazynMenu() throws IOException, RuntimeException {
		System.out.println("\n************************");
        System.out.println("1. View magazin with packages numbers");
        System.out.println("2. View magazin with packages priorities");
        System.out.println("3. View packages details");
        System.out.println("4. View only one type packages");
        System.out.println("5. View last moves history");
        System.out.println("6. Get package by number");
        System.out.println("7. Exit\n");
        System.out.print("Enter your choice: ");
        
        int selectedNumber = scanner.nextInt();
        if(selectedNumber == 7) {
        	//saveData();
        	return false;
        }
        action(selectedNumber);
        return true;
    }
	
	private void action(int selectedNumber) throws IOException, RuntimeException {
        switch (selectedNumber) {
            case 1:
                viewPackagesNumber();
                break;
            case 2:
                viewPackagesPriority();
                break;
            case 3:
                viewPackagesDetails(packages);
                break;
            case 4:
            	choosePackagesType();
                break;
            case 5:
            	viewLastMovesHistory();
                break;
            case 6:
            	choosePackageByNumber();
                break;
            default:
                break;
        }
    }
	
	public void viewLastMovesHistory() {
		for(PackageMove packageMove1 : movesHistory) {
			System.out.println(packageMove1.toString());
		}
	}
	
	public void viewPackagesDetails(List<Package> packages1) {
		packages1.sort(Comparator.comparing(Package::getNumber));
		for(Package package1 : packages1) {
			System.out.println(package1.toString());
		}
	}
	
	public void viewPackagesPriority() {
		for(Tower[] ii : tower) {
			for(Tower jj : ii) {
				System.out.print(jj.getPriority());
			}
			System.out.println();
		}
	}
	
	public void viewPackagesNumber() {
		for(Tower[] ii : tower) {
			for(Tower jj : ii) {
				System.out.print(jj.getNumber());
			}
			System.out.println();
		}
	}
	
	public void addPackagesToList() {
		for(Tower[] ii : tower) {
			for(Tower jj : ii) {
				for(Package package1 : jj.getPackages()) {
					packages.add(package1);
				}
			}
		}
	}

	public void choosePackagesType() {
		System.out.println("What type of packages do you wanna show?");
		int iterator = 1;
		for(TypeOfPackage type : TypeOfPackage.values()) {
			System.out.println(iterator++ + ". " + type);
		}
		int choice = scanner.nextInt();
		TypeOfPackage typ = TypeOfPackage.chooseEnum(choice);
		viewAllPackagesByType(typ);
	}
	
	public  List<Package> getAllPackagesByType(TypeOfPackage typ) {
		List<Package> packages1 = new ArrayList<>();
		for(Package package1 : packages) {
			if(package1.getTyp() == typ) {
				packages1.add(package1);
			}
		}
		return packages1;
	}

	public void viewAllPackagesByType(TypeOfPackage typ) {
		List<Package> packages1 = getAllPackagesByType(typ);
		viewPackagesDetails(packages1);
	}

	public void choosePackageByNumber() {
		viewPackagesDetails(packages);
		System.out.println("Enter package number:");
		int number = scanner.nextInt();
		getPackageByNumber(number);
	}
	
	public void getPackageByNumber(int number) {
		for(Package package1 : packages) {
			if(package1.getNumber() == number) {
				movesHistory = relocatePackageDoPrzerobienia(package1, package1.getX(), package1.getY(), package1.getZ());
				//packages.removeIf(i -> i.getNumber() == package1.getNumber());

			}
		}
	}
	
	public static List<PackageMove> relocatePackageDoPrzerobienia(Package package1, int x, int y, int z) {
		List<PackageMove> packagesMovesHistory = new ArrayList<>();
		int ile = tower[x][y].getPackages().size() - z;
		while (ile > 1) {
		Boolean ifMoved = true;
		int ii = 0;
		while(ifMoved && ii < tower.length) {
			int jj = 0;
			while(ifMoved && jj < tower[ii].length) {
				if(tower[ii][jj].getPackages().isEmpty() || tower[ii][jj].lastItem().getPriority() <= tower[x][y].lastItem().getPriority()) {
					if(ii != x || jj != y) {
							String oldCoordinates = tower[x][y].lastItem().coordinatesToString();
							tower[ii][jj].addPackage(tower[x][y].lastItem());
							tower[ii][jj].lastItem().setX(ii);
							tower[ii][jj].lastItem().setY(jj);
							tower[ii][jj].lastItem().setZ(tower[ii][jj].getPackages().size() - 1);
							PackageMove packageMove= new PackageMove(tower[x][y].lastItem().getNumber(), oldCoordinates, tower[ii][jj].lastItem().coordinatesToString());
							packagesMovesHistory.add(packageMove);
							tower[ii][jj].lastItem().addMove();
							tower[x][y].deletePackage();
							ifMoved = false;
					}
				}
				jj++;
			}
			ii++;
			}
		ile--;
		}
		tower[x][y].deletePackage();
		return packagesMovesHistory;
	}
	

}
