package project.magazyn.enums;

public enum TypeOfPackage {
	Toys,
	Furnitures,
	CarParts;

	public static TypeOfPackage chooseEnum(int choice) {
		switch (choice) {
		case 1:
            return TypeOfPackage.Toys;
		case 2:
            return TypeOfPackage.Furnitures;
		case 3:
            return TypeOfPackage.CarParts;
        default:
        	return TypeOfPackage.Toys;
		}
		
	}
}

